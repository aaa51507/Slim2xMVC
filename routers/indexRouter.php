<?php

/**
 * 載入Twig View的tpl
 */
$app->get('/', function () use ($app) {
    $app->render('index.html', array('hello' => "This is test call Twig View"));
});
