<?php

//auto load自動載入
use models\test\Test;
use models\member\Member;

// Get Test
$app->get('/test', function () use ($app) {
	echo "This is test router";
});

//測試router呼叫Models中的Class test
$app->get('/testSayHi/:name', function ($name) use ($app) {
	$test = new Test();
	echo $test->sayHi($name);
});

//測試router呼叫lib的php檔中的global function
$app->get('/testSayHello/:name', function ($name) use ($app) {
	$test = new Test();
	echo $test->sayHello($name);
});

// Post Test
$app->post('/test', function () use ($app) {	
	echo "test router for post";
});
