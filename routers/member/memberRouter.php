<?php

use models\member\Member;

/**
 * 取得所有會員資料
 */
$app->get('/member', function () use ($app) {	
	$Member = new Member ();
	$members = $Member->getMembers();
	$app->contentType('application/json');
	echo json_encode($members);
});

/**
 * 取得會員資料by id
 */
$app->get('/member/:id', function ($id) use ($app) {
	$Member = new Member ();
	$members = $Member->getMemberById($id);
	$app->contentType('application/json');
	echo json_encode($members);
});

/**
 * 新增會員
 */
$app->post('/member', function () use ($app) {	
	$member = json_decode($app->request()->post('data'), true);
	$Member = new Member ();
	$membersId = $Member->insertMember($member);
	$app->contentType('application/json');

	echo "insert success, memberId: $membersId";
});

// PUT route
$app->put('/member', function () {
	echo 'This is a PUT route';
});

// DELETE route
$app->delete('/member', function () {
    echo 'This is a DELETE route';
});