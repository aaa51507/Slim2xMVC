<?php

/**
 * 說hello
 * @param string $name Say hello to name
 * @return string HelloString
 */
function sayHello($name) {
	return "Hello, $name ~";
}
