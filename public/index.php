<?php

require '../vendor/autoload.php';
require '../config.php';

// Setup custom Twig view
$twigView = new \Slim\Views\Twig();

//Slim 一開始的宣告
$app = new \Slim\Slim(array(
    'debug' => true,
    'view' => $twigView,
    'templates.path' => '../templates/',
));


require '../routers/indexRouter.php';
//載入此floder下的所有檔案
$routers = glob('../routers/*/*Router.php');
foreach ($routers as $router) {
    require $router;
}

$app->run();
