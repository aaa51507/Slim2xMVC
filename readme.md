SlimMVC
=======

SlimMVC is the easiest and flexible way to create your PHP application using a MVC pattern.
SlimMVC use the PHP microframework [Slim Framework](http://www.slimframework.com/) and use the best practices collected in the slim community.

Getting Started
---------------
1. Get or download the project
2. Install it using Composer

此架構參考: https://github.com/revuls/SlimMVC

目錄結構
---------------
* lib/(存放相關引用的套件檔)
    * test/ (測試的class)
        * test.php (可存放global function)
    * ui/ (存放與ui相關的資料轉換class)
        * DataTable.php (將資料轉換成DataTable的code)
    * Config.php (設定conf參數的class)
    * Core.php (對DB連線)
* models/(存放後台程式邏輯的class檔案，命名開頭皆為大寫ex: Member.php)
    * member/(存放與會員相關的class)
        * Member.php/(取得會員資料的class)
        * Role.php/(取得會員規則的class)
    * test/(測試class)
        * Test.php/(測試用)
    * workorder/(存放與workorder相關的class)
        * WorkOrder.php(取得workorder的class)
* public/(存放前端網頁檔案)
    * .htaccess(apache轉址檔)
* routers/(存放restfulAPI，命名規則為檔案名稱加Router.php ex:memberRouter.php)
    * member/(存放與會員相關的API檔案)
        * memberRouter.php(會員API)
        * roleRouter.php(規則API)
    * mobileAPI/(存放手機所需的API檔案)
    * test/(test API)
        * testRouter.php(測試的API)
    * indexRouter.php (根目錄的API)
* templates/(Slim存放View的地方，目前使用Twig模板)
* vendor/(php composer依照composer.json的設定，去下載相依的php函式庫)

* composer.json(所需的相依檔案)
* config.php(相關config的設定)

相關學習網站
---------------
* Namespaces 與 Class Autoloading: http://ithelp.ithome.com.tw/articles/10134247
* 范围解析操作符"::" : http://php.net/manual/zh/language.oop5.paamayim-nekudotayim.php
* PHP 區別 public private protected: http://kikinote.net/article/1651.html
* self和$this的差異: http://miggo.pixnet.net/blog/post/30799978-%5Bphp%E8%A7%80%E5%BF%B5%5Dself%E5%92%8C$this%E7%9A%84%E5%B7%AE%E7%95%B0