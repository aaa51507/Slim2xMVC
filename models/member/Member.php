<?php

/**
 * This is an example of Member Class using PDO
 */

namespace models\member;
use lib\Core;
use PDO;

class Member {

	protected $core;

	function __construct() {
		$this->core = Core::getInstance();
	}
	
	/**
	 * 取得所有會員
	 * @retirm [object, object...] 會員資料
	 */
	public function getMembers() {
		$r = array();		

		$sql = "SELECT * FROM member";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);

		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}

	/**
	 * 利用會員id取得特定會員
	 * @param string $id member.id
	 * @return  test
	 */
	public function getMemberById($id) {
		$r = array();		
		
		$sql = "SELECT * from member WHERE id='$id'";
		$stmt = $this->core->dbh->prepare($sql);

		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}
	
	/**
	 * 新增會員
	 * @param object $data 會員資料
	 * @return string 新增成功的會員id
	 */
	public function insertMember($data) {
		try {
			$sql = "INSERT INTO member (icon, name, nick_name, email, password, birthday) 
					VALUES (:icon, :name, :nick_name, :email, :password, :birthday)";
			$stmt = $this->core->dbh->prepare($sql);
			
			if ($stmt->execute($data)) {
				return $this->core->dbh->lastInsertId();
			} else {
				return '0';
			}
		} catch(PDOException $e) {
        	return $e->getMessage();
    	}
	}

	// Update the data of an member
	public function updateMember($data) {
		
	}

	// Delete member
	public function deleteMember($id) {
		
	}
}