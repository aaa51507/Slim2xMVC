<?php

/**
 * This is an example of Member Class using PDO
 */

namespace models\member;
use lib\Core;
use PDO;

class Role {

	protected $core;

	function __construct() {
		$this->core = Core::getInstance();
	}
	
	/**
	 * 取得會員規則
	 * @param string $memberId 會員id
	 * @retirm object 會員規則的資料
	 */
	public function getRoles($memberId) {
		$r = array();		

		$sql = "SELECT * FROM role where $memberId";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);

		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}
}