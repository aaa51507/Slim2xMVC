<?php

/**
 * This is an example of Member Class using PDO
 */

namespace models\workorder;
use lib\Core;
use PDO;

class Member {

	protected $core;

	function __construct() {
		$this->core = Core::getInstance();
	}
	
	/**
	 * 取得所有WorkOrders
	 * @retirm [object, object...] WorkOrder Data
	 */
	public function getWorkOrders() {
		$r = array();		

		$sql = "SELECT * FROM WorkOrders";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);

		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}
}