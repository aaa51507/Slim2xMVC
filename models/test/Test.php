<?php

/**
 * This is an example of Test Class
 */

//autoload名稱定義
namespace models\test;
//auto load自動載入
use lib\Core;
use PDO;

require '../../lib/test/test.php';
require '../../lib/ui/DataTable.php';

class Test {

	//繼承此類別可以使用的變數
	//繼承的寫法 class className extends Test
	protected $core;

	/**
	 * 建構子(定義SQL連線)
	 */
	function __construct() {
		$this->core = Core::getInstance();
//		$this->core = new Core();
	}
	
	/**
	 * 說你好
	 * @parmas string $name 名字
	 * @return string 說你好的字串
	 */
	public function sayHi($name) {
		return "Hi, $name";
	}
	
	/**
	 * 說哈囉
	 * (測試class include test.php並且呼叫global function)
	 * @parmas string $name 名字
	 * @return string 說哈囉的字串
	 */
	public function sayHello($name) {
		return sayHello($name);
	}
	
	/**
	 * 測試class include DataTable.php並且呼叫global function
	 * @return string
	 */
	public function testDataTable() {
		return workOrderTransform();
	}
}